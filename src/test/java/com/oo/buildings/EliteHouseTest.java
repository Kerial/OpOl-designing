package com.oo.buildings;

import com.oo.buildings.building.EliteHouse;
import com.oo.buildings.exception.StateException;
import com.oo.buildings.states.state.EliteHouseState;
import org.junit.Test;

public class EliteHouseTest {
    @Test
    public void build() throws Exception {
        EliteHouse eliteHouse = new EliteHouse();
        eliteHouse.build();
        assert eliteHouse.getState().equals(EliteHouseState.ABANDONED);
    }

    @Test
    public void improveState() throws StateException {
        EliteHouse eliteHouse = new EliteHouse();
        eliteHouse.build();
        try {
            eliteHouse.improveState();
        } catch (StateException StateException) {
            StateException.printStackTrace();
        }
        assert eliteHouse.getState().equals(EliteHouseState.RESIDENCE);
    }

    @Test
    public void decreaseState() throws StateException {
        EliteHouse eliteHouse = new EliteHouse();
        eliteHouse.setState(EliteHouseState.ESTATE);
        try {
            eliteHouse.decreaseState();
        } catch (StateException StateException) {
            StateException.printStackTrace();
        }
        assert eliteHouse.getState().equals(EliteHouseState.MANOR);
    }
}
