package com.oo.buildings;

import com.oo.buildings.building.CommonHouse;
import com.oo.buildings.exception.StateException;
import com.oo.buildings.states.state.CommonHouseState;
import org.junit.*;

public class CommonHouseTest {
    @Test
    public void build() throws Exception {
        CommonHouse commonHouse = new CommonHouse();
        commonHouse.build();
        assert commonHouse.getState().equals(CommonHouseState.HUT);
    }

    @Test
    public void improveState() throws StateException {
        CommonHouse commonHouse = new CommonHouse();
        commonHouse.build();
        try {
            commonHouse.improveState();
        } catch (StateException StateException) {
            StateException.printStackTrace();
        }
        assert commonHouse.getState().equals(CommonHouseState.SHACK);
    }

    @Test
    public void decreaseState() throws StateException {
        CommonHouse commonHouse = new CommonHouse();
        commonHouse.setState(CommonHouseState.TOWNHOUSE);
        try {
            commonHouse.decreaseState();
        } catch (StateException StateException) {
            StateException.printStackTrace();
        }
        assert commonHouse.getState().equals(CommonHouseState.APARTMENT);
    }
}
