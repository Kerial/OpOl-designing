package com.oo.buildings.exception;

public class StateException extends Exception {
    public StateException(String message) {
        super(message);
    }
}
