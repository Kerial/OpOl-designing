package com.oo.buildings.building;

import com.oo.buildings.states.statemanager.HouseStateManager;

public class CommonHouse extends House implements WithConstruction{

    public CommonHouse(){
        setStateManager(HouseStateManager.commonHouse());
        setState(((HouseStateManager)getStateManager()).getInConstructionState());
    }

}
