package com.oo.buildings.building;

import com.oo.buildings.exception.StateException;
import com.oo.buildings.states.state.OrderedStateEnum;
import com.oo.buildings.states.statemanager.OrderedStateManager;

public abstract class Building {

    private OrderedStateEnum state;
    private OrderedStateManager<OrderedStateEnum> stateManager;

    public OrderedStateManager getStateManager() {
        return stateManager;
    }

    public void setStateManager(OrderedStateManager stateManager){
        this.stateManager = stateManager;
    }

    public OrderedStateEnum getState() {
        return state;
    }

    public void setState(OrderedStateEnum state){
        this.state = state;
    }

    public void improveState() throws StateException {
        this.state = stateManager.getNextState(this.state);
    }

    public void decreaseState() throws StateException {
        this.state = stateManager.getPreviousState(this.state);
    }

}
