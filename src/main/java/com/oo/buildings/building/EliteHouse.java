package com.oo.buildings.building;

import com.oo.buildings.states.statemanager.HouseStateManager;

public class EliteHouse extends House {

    public EliteHouse(){
        setStateManager(HouseStateManager.eliteHouse());
        setState(((HouseStateManager)getStateManager()).getInConstructionState());
    }
}
