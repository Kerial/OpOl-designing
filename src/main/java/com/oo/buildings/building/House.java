package com.oo.buildings.building;

import com.oo.resource.Resource;

import java.util.List;

public abstract class House extends Building implements WithConstruction{

    private List<Resource> resources = null;
    public List<Resource> getResources() {
        return resources;
    }

    @Override
    public void build() {
        setState(getStateManager().getFirstState());
    }
}
