package com.oo.buildings.building;

public interface WithConstruction {
    void build();
}
