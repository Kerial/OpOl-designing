package com.oo.buildings.states.state;

public enum CommonHouseState implements OrderedStateEnum {

    HUT(10), SHACK(20), HOVEL(30), HOMESTEAD(40), APARTMENT(50), TOWNHOUSE(60);

    private int index;

    CommonHouseState(final int index){
        this.index = index;
    }

    @Override
    public int getIndex(){
        return index;
    }

}
