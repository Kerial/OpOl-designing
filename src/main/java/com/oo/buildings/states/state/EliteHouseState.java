package com.oo.buildings.states.state;

public enum EliteHouseState implements OrderedStateEnum {

    ABANDONED(10), RESIDENCE(20), MANSION(30), MANOR(40), ESTATE(50);

    private int index;

    EliteHouseState(final int index){
        this.index = index;
    }

    @Override
    public int getIndex(){
        return index;
    }

}
