package com.oo.buildings.states.state;


public enum WithConstructionState implements OrderedStateEnum {
    IN_CONSTRUCTION(1);

    private int index;

    WithConstructionState(final int index){
        this.index = index;
    }

    @Override
    public int getIndex(){
        return index;
    }
}
