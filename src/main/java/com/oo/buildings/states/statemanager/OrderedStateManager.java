package com.oo.buildings.states.statemanager;

import com.oo.buildings.exception.StateException;
import com.oo.buildings.states.state.OrderedStateEnum;

import java.util.NavigableMap;

public interface OrderedStateManager<T extends OrderedStateEnum> {

    NavigableMap<Integer, T> getMapOfStates();

    /**
     * @return l'etat suivant si il existe, null sinon.
     */
    default T getNextState(T actualState) throws StateException {
        T nextState = this.getMapOfStates().higherEntry(actualState.getIndex()).getValue();
        if(nextState == null) {
            throw new StateException("No next State");
        }
        else {
            return nextState;
        }
    }

    /**
     * @return l'etat precedent si il existe, null sinon.
     */
    default T getPreviousState(T actualState) throws StateException{
        T previousState = this.getMapOfStates().lowerEntry(actualState.getIndex()).getValue();
        if(previousState == null) {
            throw new StateException("No previous State");
        }
        else {
            return previousState;
        }
    }

    default T getFirstState(){
        return this.getMapOfStates().firstEntry().getValue();
    }

    default T getLastState(){
        return this.getMapOfStates().lastEntry().getValue();
    }

}
