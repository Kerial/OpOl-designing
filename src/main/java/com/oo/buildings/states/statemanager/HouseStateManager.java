package com.oo.buildings.states.statemanager;

import com.oo.buildings.states.state.CommonHouseState;
import com.oo.buildings.states.state.EliteHouseState;
import com.oo.buildings.states.state.WithConstructionState;
import com.oo.buildings.states.state.OrderedStateEnum;

public interface HouseStateManager<T extends OrderedStateEnum> extends OrderedStateManager<T> {

    default WithConstructionState getInConstructionState(){
        return WithConstructionState.IN_CONSTRUCTION;
    }

    // Singleton
    static HouseStateManager<CommonHouseState> commonHouse() {
        return StateManagers.CommonHouseStateManager.INSTANCE;
    }

    // Singleton
    static HouseStateManager<EliteHouseState> eliteHouse() {
        return StateManagers.EliteHouseStateManager.INSTANCE;
    }
}
