package com.oo.buildings.states.statemanager;

import com.oo.buildings.states.state.CommonHouseState;
import com.oo.buildings.states.state.EliteHouseState;

import java.util.NavigableMap;
import java.util.TreeMap;

public class StateManagers {

    private StateManagers() {
        throw new AssertionError("no instances");
    }

    enum CommonHouseStateManager implements HouseStateManager<CommonHouseState> {
        INSTANCE;

        private static NavigableMap<Integer, CommonHouseState> map = new TreeMap<>();

        static {
            for (CommonHouseState state : CommonHouseState.values()) {
                map.put(state.getIndex(), state);
            }
        }

        @Override
        public NavigableMap<Integer, CommonHouseState> getMapOfStates() {
            return map;
        }

    }

    enum EliteHouseStateManager implements HouseStateManager<EliteHouseState> {
        INSTANCE;

        private static NavigableMap<Integer, EliteHouseState> map = new TreeMap<>();

        static {
            for (EliteHouseState state : EliteHouseState.values()) {
                map.put(state.getIndex(), state);
            }
        }

        @Override
        public NavigableMap<Integer, EliteHouseState> getMapOfStates() {
            return map;
        }

    }
}
